# GeoPython mit dem Jupyter Notebook - Vektordaten

## Running in OSGeo Live

Download the repository as .zip file and extract it. Run the cells in the Setup notebook first!

## Running via Binder

See `requirements.txt` for the Python packages being available.

`apt.txt` is used to install pre-compiled dependencies (e.g. `libspatialindex-dev` for `rtree`).

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/g2lab%2Ffossgis2019-geopython-vector/master)
